# powderbooking-infra

Infrastructure as code for the Powderbooking application

### Environmental variables

The CI/CD pipeline expects the following environmental variables to be set:

| Variable                      | Description                                                                  |
|-------------------------------|------------------------------------------------------------------------------|
| TF_VAR_gcp_credentials        | Your Google Credentials, base64 decoded.                                     |
| TF_VAR_grafana_admin_password | The password used to login as admin for the grafana dashboard.               |
| TF_VAR_agent_token            | The token for the Gitlab Agent*                                              |
| TF_VAR_kas_address            | The address for the Gitlab Agent*                                            |
| EMAIL                         | The email address used for let's encrypt to warn about possible TLS changes. |

Note, all variables can and should be masked.

* See [Create a Google GKE cluster](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html)