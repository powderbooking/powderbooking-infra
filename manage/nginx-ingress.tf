resource "helm_release" "nginx" {
  # https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx
  name             = "nginx"
  namespace        = "nginx-ingress"
  create_namespace = true
  chart            = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  version          = "3.15.2"
  values = [
    file("nginx-ingress-values.yaml")
  ]
  # requires the CRDs of a ServiceMonitor
  depends_on = [helm_release.prometheus]
}
