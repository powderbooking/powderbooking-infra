variable "manage_backend_address" {}
variable "manage_backend_username" {}
variable "manage_backend_password" {}

# you will need to set this as TF_VAR_gcp_credentials
# base64 <YOUR_CREDS_JSON_FILE_PATH> | tr -d '\n'
variable "gcp_credentials" {}

variable "grafana_admin_password" {
  description = "the password used to login as admin for the grafana dashboard, set through env variable"
}

variable "email" {
  description = "the email that will be used for the letsencrypt cluster issuer"
}

# Ref: https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html
variable "agent_version" {
  default     = "v15.2.0"
  description = "Agent version"
}
variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}
variable "agent_token" {
  description = "Agent token (provided when registering an Agent in GitLab)"
  sensitive   = true
}
variable "kas_address" {
  description = "Agent Server address (provided when registering an Agent in GitLab)"
}
