resource "helm_release" "prometheus" {
  name              = "prometheus"
  namespace         = "prometheus"
  create_namespace  = true
  chart             = "kube-prometheus-stack"
  repository        = "https://prometheus-community.github.io/helm-charts"
  version           = "37.3.0"
  dependency_update = true
  values = [
    file("prometheus-stack-values.yaml")
  ]

  # https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release#set_sensitive
  set_sensitive {
    name = "grafana.adminPassword"
    # TODO: random_password that gets output and stored as artifact in gitlab
    # Ref: https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password
    value = var.grafana_admin_password
  }
}