resource "google_dns_managed_zone" "powderbooking-zone" {
  name          = "powderbooking-dns-managed-zone"
  dns_name      = "powderbooking.com."
  description   = "Automatically managed zone by external-dns"
  force_destroy = true
}

resource "helm_release" "external-dns" {
  name             = "external-dns"
  namespace        = "external-dns"
  create_namespace = true
  chart            = "external-dns"
  repository       = "https://charts.bitnami.com/bitnami"
  version          = "6.7.0"
  # Update the dependencies, as described by the upgrading process
  # Ref: https://github.com/bitnami/charts/tree/master/bitnami/external-dns#to-430
  dependency_update = true
  values = [
    file("external-dns-values.yaml")
  ]
  # requires the CRDs of a ServiceMonitor
  depends_on = [helm_release.prometheus]

}
