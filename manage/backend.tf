terraform {
  backend "http" {
  }
}

data "terraform_remote_state" "provision" {
  backend = "http"

  config = {
    address        = var.manage_backend_address
    lock_address   = "${var.manage_backend_address}/lock"
    unlock_address = "${var.manage_backend_address}/lock"
    username       = var.manage_backend_username
    password       = var.manage_backend_password

    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }

}