resource "helm_release" "cm" {
  name              = "cm"
  namespace         = "cert-manager"
  create_namespace  = true
  chart             = "cert-manager"
  repository        = "https://charts.jetstack.io"
  version           = "v1.8.2"
  dependency_update = true
  values = [
    file("cert-manager-values.yaml")
  ]
  # requires the CRDs of a ServiceMonitor
  depends_on = [helm_release.prometheus]
}
