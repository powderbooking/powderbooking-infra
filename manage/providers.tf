# retrieve the GKE cluster information
provider "google" {
  # https://timberry.dev/posts/terraform-pipelines-in-gitlab/
  # https://github.com/hashicorp/terraform-provider-google/issues/704
  # https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-variables?in=terraform/gcp-get-started
  # TODO: use vault with envconsul/gitlab secrets
  credentials = base64decode(var.gcp_credentials)

  project = data.terraform_remote_state.provision.outputs.gcp_project
  region  = data.terraform_remote_state.provision.outputs.gcp_region
}

data "google_container_cluster" "primary" {
  name     = data.terraform_remote_state.provision.outputs.kubernetes_cluster_name
  location = data.terraform_remote_state.provision.outputs.gcp_region
}

# Configure kubernetes provider with Oauth2 access token.
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config
# This fetches a new token, which will expire in 1 hour.
data "google_client_config" "current" {}

provider "kubernetes" {
  host = data.terraform_remote_state.provision.outputs.kubernetes_cluster_host

  token                  = data.google_client_config.current.access_token
  cluster_ca_certificate = base64decode(data.terraform_remote_state.provision.outputs.kubernetes_certificate)
}

provider "helm" {
  kubernetes {
    host = data.terraform_remote_state.provision.outputs.kubernetes_cluster_host

    token                  = data.google_client_config.current.access_token
    cluster_ca_certificate = base64decode(data.terraform_remote_state.provision.outputs.kubernetes_certificate)
  }
}