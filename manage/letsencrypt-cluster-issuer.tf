resource "helm_release" "ci" {
  name             = "ci"
  namespace        = "cert-manager"
  create_namespace = false
  chart            = "./charts/letsencrypt-cluster-issuer"
  depends_on       = [helm_release.cm]
  set_sensitive {
    name  = "email"
    value = var.email
  }
}
