resource "helm_release" "prometheus-pushgateway" {
  name              = "prometheus-pushgateway"
  namespace         = "prometheus"
  create_namespace  = true
  chart             = "prometheus-pushgateway"
  repository        = "https://prometheus-community.github.io/helm-charts"
  version           = "1.18.2"
  dependency_update = true
  values = [
    file("prometheus-pushgateway-values.yaml")
  ]
}