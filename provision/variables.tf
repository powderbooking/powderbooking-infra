variable "gcp_project" {
  description = "the project name"
  default     = "powderbooking"
}

# you will need to set this as TF_VAR_gcp_credentials
# base64 <YOUR_CREDS_JSON_FILE_PATH> | tr -d '\n'
variable "gcp_credentials" {}

variable "gcp_region" {
  description = "the location of the VPC and kubernetes cluster"
  default     = "europe-west4"
}
variable "gcp_location" {
  description = "the location of the kubernetes cluster and node pool"
  default     = "europe-west4-a"
}

variable "min_node_count" {
  description = "the minimal amount of nodes in the node pool"
  default     = 3
}

variable "max_node_count" {
  description = "the minimal amount of nodes in the node pool"
  default     = 5
}

variable "machine_type" {
  description = "the machine type that will be used in the node pool"
  default     = "e2-small"
}
