# ensure we re-use the same variables (these are only set here and reused in /manage)
output "gcp_project" {
  value = var.gcp_project
}
output "gcp_region" {
  value = var.gcp_region
}
output "gcp_location" {
  value = var.gcp_location
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "The name of the GKE cluster that has been provisioned"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.primary.endpoint
  description = "the host on which the GKE cluster is accessible that has been provisioned"
}

output "kubernetes_certificate" {
  value = google_container_cluster.primary.master_auth.0.cluster_ca_certificate
}

output "env-dynamic-url" {
  value = "https://${google_container_cluster.primary.endpoint}"
}
