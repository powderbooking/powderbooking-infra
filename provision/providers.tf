provider "google" {
  # https://timberry.dev/posts/terraform-pipelines-in-gitlab/
  # https://github.com/hashicorp/terraform-provider-google/issues/704
  # https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-variables?in=terraform/gcp-get-started
  # TODO: use vault with envconsul/gitlab secrets
  credentials = base64decode(var.gcp_credentials)

  project = var.gcp_project
  region  = var.gcp_region
}