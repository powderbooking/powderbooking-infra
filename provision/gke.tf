resource "google_container_cluster" "primary" {
  description = "the primary kubernetes cluster that will be used to deploy the powderbooking application on."
  name        = "${var.gcp_project}-cluster"
  location    = var.gcp_location

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  remove_default_node_pool = true
  initial_node_count       = 1
  cluster_autoscaling {
    enabled = true
    resource_limits {
      resource_type = "cpu"
      minimum       = 1
      maximum       = 5
    }
    resource_limits {
      resource_type = "memory"
      minimum       = 1
      maximum       = 32
    }
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name     = "${var.gcp_project}-node-pool"
  location = var.gcp_location
  cluster  = google_container_cluster.primary.name

  autoscaling {
    max_node_count = var.max_node_count
    min_node_count = var.min_node_count
  }

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    ]
  }
}